import { Component, OnInit } from '@angular/core';
import {AuthService} from '../shared/auth.service';
import {Router} from '@angular/router';
import {AbstractControl, FormControl, FormGroup, ValidatorFn, Validators} from '@angular/forms';

export function forbiddenPasswordValidator(): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} | null => {
    const regex = /(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])/g;
    return regex.test(control.value) ? null : {'forbiddenPassword': {value: control.value}};
  };
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {

  constructor(private authService: AuthService, private router: Router) { }

  loginForm: FormGroup;

  ngOnInit() {
    this.loginForm = new FormGroup({
      'userName': new FormControl('', [
        Validators.required,
        Validators.minLength(5)
      ]),
      'password': new FormControl('', [
        Validators.required,
        Validators.minLength(8),
        forbiddenPasswordValidator()
      ]),
    });
  }

  get userName() { return this.loginForm.get('userName'); }

  get password() { return this.loginForm.get('password'); }

  logIn() {
    console.log(this.password.errors);
    if (this.authService.auth(this.userName.value, this.password.value)) {
      this.router.navigate(['/wall']);
    }
  }
}
