import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostDetailsComponent } from './post-details.component';
import {RouterTestingModule} from '@angular/router/testing';
import {PostsService} from '../shared/posts.service';
import {HttpClientModule} from '@angular/common/http';
import {AuthService} from '../shared/auth.service';
import {ModalModule} from 'ngx-bootstrap';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';

describe('PostDetailsComponent', () => {
  let component: PostDetailsComponent;
  let fixture: ComponentFixture<PostDetailsComponent>;

  let httpTestingController: HttpTestingController;
  let service: PostsService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostDetailsComponent ],
      imports: [ RouterTestingModule, HttpClientTestingModule, ModalModule.forRoot() ],
      providers: [ PostsService, AuthService ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    httpTestingController = TestBed.get(HttpTestingController);
    service = TestBed.get(PostsService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
