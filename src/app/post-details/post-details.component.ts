import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {PostsService} from '../shared/posts.service';
import {Post} from '../shared/post';
import {AuthService} from '../shared/auth.service';
import {catchError} from 'rxjs/operators';
import {of} from 'rxjs/observable/of';
import {HttpErrorResponse} from '@angular/common/http';
import {ErrorDialogComponent} from '../shared/error-dialog/error-dialog.component';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';

@Component({
  selector: 'app-post-details',
  templateUrl: './post-details.component.html',
  styleUrls: ['./post-details.component.less']
})
export class PostDetailsComponent implements OnInit {
  postDetails$;
  bsModalRef: BsModalRef;

  constructor(private route: ActivatedRoute, private postsService: PostsService,
              private authService: AuthService, private modalService: BsModalService) {}

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.postDetails$ = this.postsService.getPost(id).pipe(
      catchError((error) => {
        console.error('error loading the list of users', error);
        this.openErrorModal(error);
        return of();
      }));
  }

  logout() {
    this.authService.logout();
  }

  openErrorModal(errorMessage: HttpErrorResponse) {
    const initialState = {
      errorMessage: errorMessage.message,
    };
    this.bsModalRef = this.modalService.show(ErrorDialogComponent, {initialState});
  }
}
