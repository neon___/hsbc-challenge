import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { TooltipModule } from 'ngx-bootstrap/tooltip';


import { AppComponent } from './app.component';
import { AppRoutingModule } from './/app-routing.module';
import { LoginComponent } from './login/login.component';
import { WallComponent } from './wall/wall.component';
import { SearchComponent } from './wall/search/search.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {PostsService} from './shared/posts.service';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import { PostDetailsComponent } from './post-details/post-details.component';
import { SearchPipe } from './wall/search/search.pipe';
import { UsernamePipe } from './shared/username.pipe';
import {AuthGuard} from './shared/auth.guard';
import {AuthService} from './shared/auth.service';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import {ReactiveFormsModule} from '@angular/forms';
import { ErrorDialogComponent } from './shared/error-dialog/error-dialog.component';
import {ModalModule} from 'ngx-bootstrap';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    WallComponent,
    SearchComponent,
    PostDetailsComponent,
    SearchPipe,
    UsernamePipe,
    PageNotFoundComponent,
    ErrorDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FontAwesomeModule,
    HttpClientModule,
    ReactiveFormsModule,
    ModalModule.forRoot()
  ],
  providers: [PostsService, AuthGuard, AuthService],
  bootstrap: [AppComponent],
  entryComponents: [ErrorDialogComponent]
})
export class AppModule { }
