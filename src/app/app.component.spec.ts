import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {LoginComponent} from './login/login.component';
import {WallComponent} from './wall/wall.component';
import {SearchComponent} from './wall/search/search.component';
import {PostDetailsComponent} from './post-details/post-details.component';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {ReactiveFormsModule} from '@angular/forms';
import {SearchPipe} from './wall/search/search.pipe';
import {UsernamePipe} from './shared/username.pipe';
import {ErrorDialogComponent} from './shared/error-dialog/error-dialog.component';
import {PostsService} from './shared/posts.service';
import {AuthGuard} from './shared/auth.guard';
import {AuthService} from './shared/auth.service';
import {BrowserModule} from '@angular/platform-browser';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {HttpClientModule} from '@angular/common/http';
import {ModalModule} from 'ngx-bootstrap';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpClientTestingModule} from '@angular/common/http/testing';
describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        LoginComponent,
        WallComponent,
        SearchComponent,
        PostDetailsComponent,
        SearchPipe,
        UsernamePipe,
        PageNotFoundComponent,
        ErrorDialogComponent
      ],
      imports: [
        BrowserModule,
        RouterTestingModule,
        FontAwesomeModule,
        HttpClientTestingModule,
        ReactiveFormsModule,
        ModalModule.forRoot()
      ],
      providers: [PostsService, AuthGuard, AuthService]
    }).compileComponents();
  }));
  // it('should create the app', async(() => {
  //   const fixture = TestBed.createComponent(AppComponent);
  //   const app = fixture.debugElement.componentInstance;
  //   expect(app).toBeTruthy();
  // }));
  // it(`should have as title 'app'`, async(() => {
  //   const fixture = TestBed.createComponent(AppComponent);
  //   const app = fixture.debugElement.componentInstance;
  //   expect(app.title).toEqual('app');
  // }));
  // it('should render title in a h1 tag', async(() => {
  //   const fixture = TestBed.createComponent(AppComponent);
  //   fixture.detectChanges();
  //   const compiled = fixture.debugElement.nativeElement;
  //   expect(compiled.querySelector('h1').textContent).toContain('Welcome to app!');
  // }));
});
