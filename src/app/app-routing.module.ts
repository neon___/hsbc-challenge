import { LoginComponent } from './login/login.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {WallComponent} from './wall/wall.component';
import {PostDetailsComponent} from './post-details/post-details.component';
import {AuthGuard} from './shared/auth.guard';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';

const routes: Routes = [
  { path: '', redirectTo: '/wall', pathMatch: 'full'},
  { path: 'login', component: LoginComponent },
  { path: 'wall', component: WallComponent, canActivate: [AuthGuard] },
  { path: 'wall/:id', component: PostDetailsComponent, canActivate: [AuthGuard]},
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
