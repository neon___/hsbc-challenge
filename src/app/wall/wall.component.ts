import {Component, OnDestroy, OnInit} from '@angular/core';
import {PostsService} from '../shared/posts.service';
import {takeUntil} from 'rxjs/operators';
import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';
import {AuthService} from '../shared/auth.service';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {ErrorDialogComponent} from '../shared/error-dialog/error-dialog.component';
import {error} from 'util';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-wall',
  templateUrl: './wall.component.html',
  styleUrls: ['./wall.component.less']
})
export class WallComponent implements OnInit, OnDestroy {
  posts = [];
  private unsubscribe$: Subject<void> = new Subject<void>();
  searchText;
  bsModalRef: BsModalRef;

  constructor(private postsService: PostsService, private authService: AuthService, private modalService: BsModalService) { }

  ngOnInit() {
    this.postsService.getPosts()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe( data => {
      this.posts = [];
      const sortedPosts = (data.sort((a, b) => b.id - a.id));
      this.addWithDelay(sortedPosts, this.posts);
    }, errorData => {
        this.openErrorModal(errorData);
      });
  }

  private addWithDelay(inputArray: any[], outputArray: any[]) {
    outputArray.push(inputArray[0]);
    let index = 1;
    const timer = setInterval(() => {
      outputArray.push(inputArray[index]);
      index += 1;
      if (index === inputArray.length) {
        clearInterval(timer);
      }
    }, 1000);
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  onSearchTextChange(event: string) {
    this.searchText = event;
    console.log(event);
  }

  logout() {
    this.authService.logout();
  }

  openErrorModal(errorMessage: HttpErrorResponse) {
    const initialState = {
      errorMessage: errorMessage.message,
    };
    this.bsModalRef = this.modalService.show(ErrorDialogComponent, {initialState});
  }
}
