import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WallComponent } from './wall.component';
import {SearchComponent} from './search/search.component';
import {SearchPipe} from './search/search.pipe';
import {UsernamePipe} from '../shared/username.pipe';
import {RouterTestingModule} from '@angular/router/testing';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {PostsService} from '../shared/posts.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AuthService} from '../shared/auth.service';
import {ModalModule} from 'ngx-bootstrap';

describe('WallComponent', () => {
  let component: WallComponent;
  let fixture: ComponentFixture<WallComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WallComponent, SearchComponent, SearchPipe, UsernamePipe ],
      imports: [ RouterTestingModule, FontAwesomeModule, HttpClientTestingModule, ModalModule.forRoot() ],
      providers: [ SearchPipe, UsernamePipe, PostsService, AuthService ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
