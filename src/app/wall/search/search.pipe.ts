import { Pipe, PipeTransform } from '@angular/core';
import {Post} from '../../shared/post';

@Pipe({
  name: 'search',
  pure: false
})
export class SearchPipe implements PipeTransform {

  transform(value: Post[], searchText: any): any {
    if (!searchText) { return value; }
    searchText = searchText.toLowerCase();
    return this.searchInArray(value, searchText);
  }

  searchInArray(posts: Array<Post>, searchText: string) {
    const hasText = posts.filter( post => {
      return post.title.toLowerCase().includes(searchText) || post.body.toLowerCase().includes(searchText);
    });
    return hasText;
  }
}
