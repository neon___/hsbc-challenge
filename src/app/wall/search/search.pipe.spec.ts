import { SearchPipe } from './search.pipe';
import {Post} from '../../shared/post';

describe('SearchPipe', () => {
  it('create an instance', () => {
    const pipe = new SearchPipe();
    expect(pipe).toBeTruthy();
  });

  it('should retrieve searched post', () => {
    const dummyPosts: Array<Post> = [{
      body: 'quia et suscipit↵suscipit recusandae consequuntur expedita et cum↵reprehenderit molestiae ut ut quas totam↵nostrum rerum',
      id: 1,
      title: 'subject 1',
      userId: 1,
    },
      {
        body: 'subject tempore vitae↵sequi sint nihil reprehenderit dolor beatae ea dolores neque↵fugiat blanditiis voluptate porro vel ',
        id: 2,
        title: 'tile',
        userId: 1
      },
      {
        body: 'et iusto sed quo iure↵voluptatem occaecati omnis eligendi aut ad↵voluptatem doloribus vel accusantium quis pariatur↵',
        id: 3,
        title: 'title',
        userId: 1,
      }];
    const pipe = new SearchPipe();
    expect((<Array<Post>>pipe.transform(dummyPosts, 'subject')).length).toEqual(2);
  });
});
