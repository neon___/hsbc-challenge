import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import {Subject} from 'rxjs/Subject';
import {debounceTime} from 'rxjs/operators';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.less']
})
export class SearchComponent implements OnInit {
  faSearch = faSearch;
  @Output() change = new EventEmitter<string>();
  private subject: Subject<string> = new Subject();

  constructor() { }

  ngOnInit() {
    this.subject.pipe(debounceTime(500)).subscribe(searchTextValue => {
      this.change.emit(searchTextValue);
    });
  }

  onInputChange(event: KeyboardEvent) {
    const searchTextValue = (event.target as HTMLInputElement).value;
    this.subject.next(searchTextValue);
  }

}
