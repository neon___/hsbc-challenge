import { Component, OnInit } from '@angular/core';
import {BsModalRef} from 'ngx-bootstrap';

@Component({
  selector: 'app-error-dialog',
  templateUrl: './error-dialog.component.html',
  styleUrls: ['./error-dialog.component.less']
})
export class ErrorDialogComponent implements OnInit {
  closeBtnName = 'Close';
  errorMessage = '';

  constructor(public bsModalRef: BsModalRef) {}

  ngOnInit() {
  }
}
