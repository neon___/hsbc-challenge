import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {Post} from './post';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class PostsService {

  constructor(private http: HttpClient) {
  }

  getPosts(): Observable<Array<Post>> {
    return this.http.get('https://jsonplaceholder.typicode.com/posts')
      .pipe(map(response => <Array<Post>>response));
  }

  getPost(id: string): Observable<Post> {
    return this.http.get(`https://jsonplaceholder.typicode.com/posts/${id}`)
      .pipe(map(response => <Post>response));
  }

}
