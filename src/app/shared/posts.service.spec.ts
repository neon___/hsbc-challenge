import {TestBed, inject} from '@angular/core/testing';

import {PostsService} from './posts.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {Post} from './post';

describe('PostsService', () => {
  let httpMock: HttpTestingController;
  const url = 'https://jsonplaceholder.typicode.com/posts';
  const dummyPosts: Array<Post> = [{
    body: 'quia et suscipit↵suscipit recusandae consequuntur expedita et cum↵reprehenderit molestiae ut ut quas totam↵nostrum rerum',
    id: 1,
    title: 'sunt aut facere repellat provident occaecati excepturi optio reprehenderit',
    userId: 1,
  },
    {
      body: 'est rerum tempore vitae↵sequi sint nihil reprehenderit dolor beatae ea dolores neque↵fugiat blanditiis voluptate porro vel ',
      id: 2,
      title: 'qui est esse',
      userId: 1
    },
    {
      body: 'et iusto sed quo iure↵voluptatem occaecati omnis eligendi aut ad↵voluptatem doloribus vel accusantium quis pariatur↵',
      id: 3,
      title: 'ea molestias quasi exercitationem repellat qui ipsa sit aut',
      userId: 1,
    }];

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PostsService],
      imports: [HttpClientTestingModule]
    });
    afterEach(() => {
      httpMock.verify();
    });

    httpMock = TestBed.get(HttpTestingController);
  });

  it('should be created', inject([PostsService], (service: PostsService) => {
    expect(service).toBeTruthy();
  }));

  it('should retrieve posts', inject([PostsService], (service: PostsService) => {
    service.getPosts().subscribe(posts => {
      expect(posts.length).toBe(3);
      expect(posts).toEqual(dummyPosts);
    });

    const request = httpMock.expectOne( url);
    expect(request.request.method).toBe('GET');
    request.flush(dummyPosts);
  }));

  it('should retrieve one post', inject([PostsService], (service: PostsService) => {
    const postId = '3';
    service.getPost(postId).subscribe(posts => {
      expect(posts).toEqual(dummyPosts[2]);
    });

    const newUrl = url + '/' + postId;
    const request = httpMock.expectOne( newUrl );
    expect(request.request.method).toBe('GET');
    request.flush(dummyPosts[2]);
  }));
});
