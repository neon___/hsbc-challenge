import { UsernamePipe } from './username.pipe';

describe('UsernamePipe', () => {
  it('create an instance', () => {
    const pipe = new UsernamePipe();
    expect(pipe).toBeTruthy();
  });

  it('create username based on userId', () => {
    const pipe = new UsernamePipe();
    expect(pipe.transform(12)).toEqual('User12');
  });
});
