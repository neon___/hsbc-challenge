import { TestBed, async, inject } from '@angular/core/testing';

import { AuthGuard } from './auth.guard';
import {AuthService} from './auth.service';
import {RouterTestingModule} from '@angular/router/testing';

describe('AuthGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ RouterTestingModule ],
      providers: [AuthGuard, AuthService]
    });
  });

  it('should create', inject([AuthGuard], (guard: AuthGuard) => {
    expect(guard).toBeTruthy();
  }));

  it('should not activate', inject([AuthGuard], (guard: AuthGuard) => {
    expect(guard.canActivate()).toBeFalsy();
  }));

  it('should activate', inject([AuthGuard, AuthService], (guard: AuthGuard, auth: AuthService) => {
    auth.auth('some name', 'some pass');
    expect(guard.canActivate()).toBeTruthy();
  }));

});
