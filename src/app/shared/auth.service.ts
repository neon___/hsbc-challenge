import { Injectable } from '@angular/core';
import {Router} from '@angular/router';

@Injectable()
export class AuthService {

  constructor(private router: Router) { }

  isAuthenticated() {
    return sessionStorage.getItem('appUserName') && sessionStorage.getItem('appUserPassword');
  }

  auth(userName: string, password: string): boolean {
    if (userName && password) {
      sessionStorage.setItem('appUserName', userName);
      sessionStorage.setItem('appUserPassword', password);
      return true;
    }
    return false;
  }

  logout() {
    sessionStorage.clear();
    this.router.navigate(['/login']);
  }
}
