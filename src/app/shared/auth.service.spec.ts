import {TestBed, inject, getTestBed} from '@angular/core/testing';

import { AuthService } from './auth.service';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpTestingController} from '@angular/common/http/testing';

describe('AuthService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ RouterTestingModule ],
      providers: [AuthService]
    });
  });

  it('should be created', inject([AuthService], (service: AuthService) => {
    expect(service).toBeTruthy();
  }));

  it('should authenticate', inject([AuthService], (service: AuthService) => {
    const pass = 'SomePass01';
    const name = 'some name';
    service.auth(name, pass);
    expect(sessionStorage.getItem('appUserName')).toEqual(name);
    expect(sessionStorage.getItem('appUserPassword')).toEqual(pass);
    expect(service.isAuthenticated()).toBeTruthy();
  }));

  it('should logout', inject([AuthService], (service: AuthService) => {
    const pass = 'SomePass01';
    const name = 'some name';
    service.auth(name, pass);
    expect(service.isAuthenticated()).toBeTruthy();
    service.logout();
    expect(service.isAuthenticated()).toBeFalsy();
  }));
});
